Categories:Science & Education
License:GPLv3
Web Site:https://github.com/mannd/morbidmeter-android
Source Code:https://github.com/mannd/morbidmeter-android
Issue Tracker:

Auto Name:MorbidMeter
Summary:Life expectancy widget
Description:
Morbid Meter is a widget that puts your time on earth into perspective. Choose a life-expectancy and a timescale and the widget will display how much of that timescale you have already lived. The default is years, so that will show what month of the year it is, so to speak. Choose it to be reversed, with seconds, and it will show you how many more seconds you can be expected to live for.
.

Repo Type:git
Repo:https://github.com/mannd/morbidmeter-android.git

Build Version:1.2.1,4,48f92

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.2.1
Current Version Code:4

