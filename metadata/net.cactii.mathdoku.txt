Categories:Games
License:GPLv3
Web Site:http://code.google.com/p/mathdoku/
Source Code:http://code.google.com/p/mathdoku/source/checkout
Issue Tracker:http://code.google.com/p/mathdoku/issues/list

Auto Name:MathDoku
Summary:Sudoku-like game based on KenKen
Description:
A sudoku-like game with an arithmetical twist. Based on KenKen(TM).
.

Repo Type:git-svn
Repo:http://mathdoku.googlecode.com/svn;trunk=trunk;tags=tags

Build Version:1.8,70,version-1.8,target=android-10
Build Version:1.95d,77,96,init=rm -rf bin/ gen/,target=android-17,\
prebuild=sed -i 's/minSdkVersion=\"3\"/minSdkVersion=\"4\" targetSdkVersion=\"4\"/g' AndroidManifest.xml
Build Version:1.96.2,281,version-1.96.2,init=rm -rf bin/ gen/,\
prebuild=sed -i 's/minSdkVersion=\"3\"/minSdkVersion=\"4\"/' AndroidManifest.xml

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.96.2
Current Version Code:281

