Categories:System
License:Apache2
Web Site:http://code.google.com/p/hackerskeyboard/
Source Code:http://code.google.com/p/hackerskeyboard/source/checkout
Issue Tracker:http://code.google.com/p/hackerskeyboard/issues/list

Name:Hacker's Keyboard
Auto Name:Hacker's Keyboard
Summary:Four- or five-row soft-keyboard
Description:
There are about thirty different languages covered, which can be enabled 
in the settings. 
For the five-row layout, the keyboard has separate number keys, punctuation in 
the usual places; tab, ctrl and arrow keys. 
It is based on the AOSP Gingerbread soft keyboard, so it supports multitouch 
for the modifier keys.

Completion dictionaries are only possible via plug-in packages, available from 
the website, though there is no template for building these from source.
Anysoftkeyboard dictionaries don't appear to work.

The permissions requested by the application are those needed by the underlying 
Gingerbread keyboard. 
It uses the contacts information for completion of names and email addresses, 
and audio recording for the voice input feature.
.

Repo Type:hg
Repo:https://hackerskeyboard.googlecode.com/hg/

Build Version:v1.18,1018,8298cd8728c2,subdir=java,target=android-11,buildjni=yes
Build Version:v1.20,1020,e703fa82a4c3,subdir=java,target=android-11,buildjni=yes
Build Version:v1.22,1022,154e21230e81,subdir=java,target=android-11,buildjni=yes
Build Version:v1.29,1029,1.29,subdir=java,target=android-11,buildjni=yes,prebuild=./AutoVersion.sh
Build Version:v1.31,1031,065ed34b811c,subdir=java,target=android-11,buildjni=yes,prebuild=sed -i "s/Hg-ident/hg ident/" AutoVersion.sh && ./AutoVersion.sh
Build Version:v1.33,1033,v1.33,subdir=java,target=android-11,buildjni=yes,prebuild=sed -i "s/Hg-ident/hg ident/" AutoVersion.sh && ./AutoVersion.sh

Auto Update Mode:None
# Tags + filter
Update Check Mode:Static
Current Version:v1.33
Current Version Code:1033

