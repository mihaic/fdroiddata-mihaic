Categories:Internet
License:NewBSD/GPLv2
Web Site:http://code.google.com/p/ics-openvpn/
Source Code:http://code.google.com/p/ics-openvpn/source/checkout
Issue Tracker:http://code.google.com/p/ics-openvpn/issues/list

Auto Name:OpenVPN for Android
Summary:OpenVPN without root
Description:
With the VPNService in Android 4.0+ it is possible to create a VPN that doesn't
need root access. The GUI is licensed under BSD while the OpenVPN core is licensed
under the GPL.
.

Repo Type:hg
Repo:https://code.google.com/p/ics-openvpn/

Build:0.5.21,48
    disable=broken v0.5.21
    commit=unknown - see disabled
    buildjni=yes

Build:0.5.22,49
    disable=broken v0.5.22
    commit=unknown - see disabled
    buildjni=yes

Build:0.5.24,51
    disable=broken too v0.5.24
    commit=unknown - see disabled
    target=android-14
    prebuild=sed -i 's/ndk-build APP_API=all -j 8/ndk-build APP_ABI=all -j 2/g' build-native.sh && \
        ./build-native.sh
    buildjni=no

Build:0.5.35,63
    disable=broken too v0.5.35
    commit=unknown - see disabled
    target=android-17
    prebuild=sed -i 's/-j 8/-j 2/g' build-native.sh && \
        ./build-native.sh
    buildjni=no

Build:0.5.47,80
    commit=v0.5.47
    gradle=yes
    rm=assets
    prebuild=sed -i '/google-breakpad/d' jni/Android.mk && \
        sed -i -e '/+=.*breakpad/d' -e 's/ [^ ]*breakpad[^ ]*//' openvpn/Android.mk && \
        find openvpn/src -type f -print0 | xargs -0 sed -i '/breakpad/d' && \
        rm -f openvpn/src/openvpn/breakpad.*
    build=echo WITH_BREAKPAD=0 >> jni/Android.mk && \
        sed -i 's/-j 8/-j 4/g' misc/build-native.sh && \
        ./misc/build-native.sh
    buildjni=no

Build:0.6.0,81
    commit=v0.6.0
    gradle=yes
    prebuild=sed -i '/google-breakpad/d' jni/Android.mk && \
        sed -i -e '/+=.*breakpad/d' -e 's/ [^ ]*breakpad[^ ]*//' openvpn/Android.mk && \
        find openvpn/src -type f -print0 | xargs -0 sed -i '/breakpad/d' && \
        rm -f openvpn/src/openvpn/breakpad.* && \
        mv src/de/blinkt/openvpn/fragments/SeekbarTicks.java src/de/blinkt/openvpn/fragments/SeekBarTicks.java
    scandelete=assets
    build=echo WITH_BREAKPAD=0 >> jni/Android.mk && \
        sed -i 's/-j 8/-j 4/g' misc/build-native.sh && \
        ./misc/build-native.sh
    buildjni=no

Build:0.6.1,82
    commit=v0.6.1
    gradle=yes
    prebuild=sed -i '/google-breakpad/d' jni/Android.mk && \
        sed -i -e '/+=.*breakpad/d' -e 's/ [^ ]*breakpad[^ ]*//' openvpn/Android.mk && \
        find openvpn/src -type f -print0 | xargs -0 sed -i '/breakpad/d' && \
        rm -f openvpn/src/openvpn/breakpad.*
    scandelete=assets
    build=echo WITH_BREAKPAD=0 >> jni/Android.mk && \
        sed -i 's/-j 8/-j 4/g' misc/build-native.sh && \
        ./misc/build-native.sh
    buildjni=no

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.6.1
Current Version Code:82

