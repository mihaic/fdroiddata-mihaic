AntiFeatures:UpstreamNonFree
Categories:Multimedia
License:GPLv2
Web Site:
Source Code:https://github.com/Teaonly/android-eye
Issue Tracker:https://github.com/Teaonly/android-eye/issues

Auto Name:Wifi Camera
Summary:Internet webcam
Description:
Supports Mjpeg and audio streaming.
Built-in web service to see the video via browser on a PC.

The Google ad library was removed.
.

Repo Type:git
Repo:https://github.com/Teaonly/android-eye.git

Build Version:1.6.1,8,f5279ae,srclibs=MobAdMob@2d5736,\
target=android-16,rm=libs/GoogleAdMobAdsSdk-6.2.1.jar,prebuild=\
echo "android.library.reference.1=$$MobAdMob$$" >> project.properties,buildjni=jni

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.6.1
Current Version Code:8

