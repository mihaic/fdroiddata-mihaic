Disabled:Lots of jars
Categories:None
License:GPLv3
Web Site:http://code.google.com/p/martus
Source Code:http://code.google.com/p/martus/source/checkout?repo=martus-android
Issue Tracker:http://code.google.com/p/martus/issues/list

Auto Name:Martus
Summary:
Description:
...
.

Repo Type:hg
Repo:https://code.google.com/p/martus.martus-android/

Build:4.4,4
    commit=PlayStoreVersion4
    subdir=martus-android
    forceversion=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2013.10.29 - 4.4
Current Version Code:4

