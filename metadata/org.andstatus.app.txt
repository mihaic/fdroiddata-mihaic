Categories:Internet
License:Apache2
Web Site:http://andstatus.org/
Source Code:https://github.com/andstatus/andstatus
Issue Tracker:https://github.com/andstatus/andstatus/issues

Auto Name:AndStatus
Summary:Light-weight Microblogger
Description:
Andstatus is a lightweight microblogging client, supporting Twitter, statusnet
and pump.io (including identi.ca). It allows you to read messages, send status
updates and create favorites, even when you are offline.

It features multiple User accounts in all supported Microblogging systems and
allows you to work offline, including reading and sending messages while you
are offline

Messages can be stored on the local storage indefinitely and the search option
is limited to that.
.

Repo Type:git
Repo:git://github.com/andstatus/andstatus.git

Build:1.9.1,61
    commit=92a6933279
    target=android-15
    extlibs=android/android-support-v4.jar

Build:1.10.0,62
    commit=eb0a0fb51c
    target=android-15
    extlibs=android/android-support-v4.jar

Build:1.10.2,64
    disable=not a release
    commit=unknown - see disabled

Build:1.11.4,69
    commit=2e3325dd77
    target=android-15
    extlibs=android/android-support-v4.jar

Build:1.12.0,70
    commit=AndStatus-1.12.0
    target=android-15
    extlibs=android/android-support-v4.jar

Build:1.13.1,72
    commit=AndStatus-1.13.1
    target=android-15
    extlibs=android/android-support-v4.jar

Build:1.14.0,73
    commit=AndStatus-1.14.0
    extlibs=android/android-support-v4.jar

Build:1.15.1,75
    commit=AndStatus-1.15.1
    extlibs=android/android-support-v4.jar

Build:1.16.0,77
    commit=AndStatus-1.16.0
    extlibs=android/android-support-v4.jar

Build:1.17.0,78
    commit=AndStatus-1.17.0
    extlibs=android/android-support-v4.jar

Build:1.17.1,79
    commit=AndStatus-1.17.1
    extlibs=android/android-support-v4.jar

Build:1.18.1,81
    commit=AndStatus-1.18.1
    extlibs=android/android-support-v4.jar

Build:2.0,83
    commit=AndStatus-2.0
    extlibs=android/android-support-v4.jar

Build:2.1,84
    commit=AndStatus-2.1
    extlibs=android/android-support-v4.jar

Build:3.0,85
    commit=AndStatus-3.0
    extlibs=android/android-support-v4.jar

Build:4.0,86
    commit=AndStatus-4.0
    extlibs=android/android-support-v4.jar

Build:5.1,89
    commit=AndStatus-5.1
    extlibs=android/android-support-v4.jar

Maintainer Notes:
Beware disappearing tags.
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:5.1
Current Version Code:89

