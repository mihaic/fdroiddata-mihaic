Disabled:pre-alpha abandonware
Category:System
License:GPLv3
Web Site:http://code.google.com/p/quitetaskmanager/
Source Code:http://code.google.com/p/quitetaskmanager/source/checkout
Issue Tracker:http://code.google.com/p/quitetaskmanager/issues/list

Auto Name:QuiteTaskManager
Summary:Lightweight task manager
Description:
Simple task/process manager with task kill support.
.

Repo Type:git-svn
Repo:http://quitetaskmanager.googlecode.com/svn/trunk/

Build Version:1.2,2,14

Auto Update Mode:None
#not in market
Update Check Mode:Static
Current Version:1.2
Current Version Code:2

