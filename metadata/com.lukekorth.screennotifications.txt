Categories:System
License:GPLv3
Web Site:http://lukekorth.com
Source Code:https://github.com/lkorth/screen-notifications
Issue Tracker:https://github.com/lkorth/screen-notifications/issues

Auto Name:Screen Notifications
Summary:Notification helper
Description:
Have the screen turn on when you get a notification of a certain kind.
You need to allow
the app as an Accessibility helper in the System preferences.
If the phone is in your pocket or
the proximity sensor is otherwise covered, the screen will not turn on.
.

Repo Type:git
Repo:https://github.com/lkorth/screen-notifications.git

Build Version:0.71,9,01e3772b86,submodules=yes,update=.;numberpicker/lib
Build Version:0.76,11,1307179a28,submodules=yes,update=.;numberpicker/lib,prebuild=\
sed -i 's@\(reference.1=\).*@\1numberpicker/lib@' project.properties
Build Version:0.81,14,43b9e17d8,submodules=yes,\
update=.;numberpicker/lib;android-donations-lib/org_donations,prebuild=\
cp libs/android-support-v4.jar android-donations-lib/org_donations/libs/ && \
mv libs/android-support-v4.jar ez-loaders/libs/ && \
sed -i 's@\(reference.1=\).*@\1numberpicker/lib@' project.properties

Auto Update Mode:None
# Also on gplay
Update Check Mode:RepoManifest
Current Version:0.81
Current Version Code:14

