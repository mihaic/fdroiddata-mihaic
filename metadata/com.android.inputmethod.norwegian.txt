Disabled:Can't build dictionary libraries
Categories:System
License:Apache2
Web Site:http://code.google.com/p/scandinavian-keyboard/
Source Code:http://code.google.com/p/scandinavian-keyboard/source/checkout
Issue Tracker:http://code.google.com/p/scandinavian-keyboard/issues/list
Donate:http://code.google.com/p/scandinavian-keyboard/

Auto Name:Scandinavian keyboard
Summary:Keyboard for Scandinavian languages
Description:
A modified version of the standard onscreen keyboard in Android
with support for Norwegian, Swedish, Danish, Faroese, German,
Icelandic and Northern Sámi keyboard layouts.
.

#Switched from svn to git
Repo Type:git
Repo:http://code.google.com/p/scandinavian-keyboard

#Build Version:1.4.4,13,15,target=android-4
#Build Version:1.4.6,15,17,target=android-4
#For android-15 only
Build Version:1.4.7,16,9515ee354b59,target=android-4

Auto Update Mode:None
#Device-variable
Update Check Mode:Static
Current Version:1.4.6
Current Version Code:15

