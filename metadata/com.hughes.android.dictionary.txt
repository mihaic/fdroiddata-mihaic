Categories:Office
License:Apache2
Web Site:http://code.google.com/p/quickdic-dictionary/
Source Code:http://code.google.com/p/quickdic-dictionary/source/list
Issue Tracker:http://code.google.com/p/quickdic-dictionary/issues/list

Auto Name:QuickDic
Summary:Offline translation dictionary
Description:
Uses data from Wiktionary and Beolingus to generate dictionary files that can
be used offline. These can be downloaded from within the app.
Learn how to construct your own dictionaries by viewing
the wiki page on the website.
.

Repo Type:git
Repo:https://code.google.com/p/quickdic-dictionary.dictionary/

Build Version:3.0.1,15,6507667d0e0af60c201ae6c732c15e2fd07886ae,prebuild=mkdir libs && \
mv jars/*.jar libs/ && \
git clone https://code.google.com/p/quickdic-dictionary.util/ Util && \
cd Util && \
git checkout c848279fde9af59fdd167eacbc5deb0258223c8e && \
cd .. && \
cp -r Util/src . && \
rm -rf Util/
Build Version:3.1,16,64d2b5,prebuild=mkdir libs && \
mv jars/*.jar libs/ && \
git clone https://code.google.com/p/quickdic-dictionary.util/ Util && \
cd Util && \
git checkout c8f5ba9eac5f110d574ef8b443a205051026688c && \
cd .. && \
cp -r Util/src . && \
rm -rf Util/
Build Version:3.2,18,115509,prebuild=mkdir libs && \
mv jars/*.jar libs/ && \
git clone https://code.google.com/p/quickdic-dictionary.util/ Util && \
cd Util && \
git checkout c8f5ba9eac5f110d574ef8b443a205051026688c && \
cd .. && \
cp -r Util/src . && \
rm -rf Util/
Build Version:3.2.1,19,66affc8fb893,prebuild=mkdir libs && \
mv jars/*.jar libs/ && \
git clone https://code.google.com/p/quickdic-dictionary.util/ Util && \
cd Util && \
git checkout c8f5ba9eac5f110d574ef8b443a205051026688c && \
cd .. && \
cp -r Util/src . && \
rm -rf Util/
Build Version:3.3,21,b50c9fe,prebuild=mkdir libs && \
mv jars/*.jar libs/ && \
git clone https://code.google.com/p/quickdic-dictionary.util/ Util && \
cd Util && \
git checkout 2781abb3e8f4 && \
cd .. && \
cp -r Util/src . && \
rm -rf Util/
Build Version:4.0.1,23,1950e66885eb,rm=Dictionary_signed.apk,\
init=git clone https://code.google.com/p/quickdic-dictionary.util/ Util,prebuild=mkdir libs && \
sed -i 's@\(android.library.reference.1=\).*@\1Util@' project.properties && \
cd jars/icu4j-4_8_1_1/ && sed -i '108d' build.xml && ant clean collator transliterator moduleJar && \
mv icu4j.jar ../../libs/

Auto Update Mode:None
#Cross reference with version of apk in root directory
Update Check Mode:RepoManifest
Current Version:4.0.1
Current Version Code:23

