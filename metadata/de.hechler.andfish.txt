Category:Games
License:Apache2
Source Code:http://svn.code.sf.net/p/feri/svn/trunk/AndFish

Summary:Simple game
Description:
Guide your fish to eat the other fish and grow bigger.
.

Repo Type:svn
Repo:http://svn.code.sf.net/p/feri/svn/trunk/AndFish

Build:1.3,20
    disable=Wrong version code and doesn't work anyway
    commit=6

Update Check Mode:None
Current Version:1.3
Current Version Code:6

