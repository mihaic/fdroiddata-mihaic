Categories:System
License:GPLv3+
Web Site:http://github.com/nexes/Android-File-Manager
Source Code:http://github.com/nexes/Android-File-Manager
Issue Tracker:http://github.com/nexes/Android-File-Manager/issues

Auto Name:Open Manager
Summary:A simple file browser and manager
Description:
A simple file browser and manager. Among other features it allows to perform a backup to the SD card of all your downloaded (third party) applications.
.

Repo Type:git
Repo:https://github.com/nexes/Android-File-Manager.git

Build Version:2.0.0,4,f55ed171435570fa8d72
Build Version:2.1.2,212,7bca6266be731085ac92
Build Version:2.1.8,218,edf793a782861ae31648

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.1.8
Current Version Code:218

