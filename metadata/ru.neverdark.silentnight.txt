Categories:Office
License:GPLv3
Source Code:https://github.com/yankovskiy/SilentNight
Issue Tracker:https://github.com/yankovskiy/SilentNight/issues

Summary:Tell the phone to mute
Description:
Configure the phone to go silent and/or airplane mode
between two times of the day.
.

Repo Type:git
Repo:https://github.com/yankovskiy/SilentNight.git

Build:0.1.5,6
    commit=v0.1.5

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.1.5
Current Version Code:6

