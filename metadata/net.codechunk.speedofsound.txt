Categories:Multimedia
License:GPL
Web Site:https://github.com/jpeddicord/speedofsound
Source Code:https://github.com/jpeddicord/speedofsound
Issue Tracker:https://github.com/jpeddicord/speedofsound/issues

Summary:Adjust volume according to speed
Description:
'''N.B''' The app no longer requires Google Maps to install but the app will
crash if you try to access the maps without having it.

'''N.B''' It isn't possible to view your position on a map because
Google Maps is the only map source and
the API keys are not in the source code. If you need to use Google Maps
install the developer's build
from the website.

Keep your eyes on the road, not your phone! Speed of Sound automatically
adjusts your music player’s volume while you’re driving about with your GPS,
lowering the volume while driving slowly or at a stoplight,
and cranking it up on the highway.

This is a feature available in some more expensive cars and sound
systems. We’re giving it to you for free to use on your phone. Just start
up your music player, plug your phone into your car stereo, and open Speed of Sound.
.

Repo Type:git
Repo:https://github.com/jpeddicord/speedofsound.git

Build Version:0.8.1,8,8dc2de2691b10101f65d9495db6d8c19d3a9f62f,subdir=speedofsound,\
update=.;../actionbarsherlock,target=Google Inc.:Google APIs:15

Build Version:0.8.2,9,0.8.2,subdir=speedofsound,\
update=.;../actionbarsherlock,target=Google Inc.:Google APIs:15,prebuild=\
sed -i 's/<uses-library android:name="com.google.android.maps" \/>/<uses-library android:name="com.google.android.maps" android:required="false" \/>/g' AndroidManifest.xml

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.8.2
Current Version Code:9

