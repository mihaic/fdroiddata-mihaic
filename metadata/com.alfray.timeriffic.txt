Categories:Office
License:GPLv3
Web Site:http://code.google.com/p/timeriffic/
Source Code:http://code.google.com/p/timeriffic/source/checkout
Issue Tracker:http://code.google.com/p/autosettings/issues/list

Summary:Scheduled settings
Description:
Allows you to set multiple schedules to control mute, vibrate, brightness,
WiFi and airplane mode.

Status: Source code was last published in 2011.
.

Build Version:1.09.03,10903,!huge ass version name at 45,subdir=Timeriffic,oldsdkloc=yes

#This version was from https://timeriffic.googlecode.com/hg/
Build Version:1.09.05,10905,fc40dccbb9,subdir=Timeriffic,oldsdkloc=yes,novcheck=yes,bindir=Timeriffic/bin2

Build Version:1.10.01,11001,!Source code is not published

Auto Update Mode:None
Update Check Mode:Static
Current Version:1.09.05
Current Version Code:10905

No Source Since:1.09.11

