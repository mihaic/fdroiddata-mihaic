Categories:Navigation
License:GPLv3
Web Site:http://code.google.com/p/geopaparazzi/
Source Code:http://code.google.com/p/geopaparazzi/source/list
Issue Tracker:http://code.google.com/p/geopaparazzi/issues/list
Donate:http://code.google.com/p/geopaparazzi/

Auto Name:GeoPaparazzi
Summary:A field survey application
Description:
Geopaparazzi is a tool developed to do fast qualitative technical and
scientific surveys. Its strength is its direct connection to the BeeGIS GIS,
that can be used to further process the collected data. Even if the main aim
is in the field of surveying, it contains tools that can be of great use also
to tourists that want to keep a geo-diary.

There is a lot of info on the wiki if you need help getting the most out of
it. It uses
OSM maps by default and the current location can be opened in [[org.mixare]].
Unfortunately,
the suggested compass app is not free software.
.

Repo Type:git
Repo:https://code.google.com/p/geopaparazzi

Build Version:3.1.1,30,version_3.1.1,subdir=geopaparazzi.app,\
update=.;../geopaparazzilibrary
#New libraries in libs/
#Can't find tag for this even though update check mode got it
Build Version:3.2.0,31,6d0834dffbf5,subdir=geopaparazzi.app,\
update=.;../geopaparazzilibrary
Build Version:3.2.1,32,version_3.2.1,subdir=geopaparazzi.app,\
update=.;../geopaparazzilibrary
Build Version:3.2.3,34,version_3.2.3,subdir=geopaparazzi.app,\
update=.;../geopaparazzilibrary
Build Version:3.2.6,37,version_3.2.6,subdir=geopaparazzi.app,update=.;../geopaparazzilibrary
Build Version:3.2.7,38,version_3.2.7,subdir=geopaparazzi.app,update=.;../geopaparazzilibrary
Build Version:3.3.0,39,version_3.3.0,subdir=geopaparazzi.app,update=.;../geopaparazzilibrary
Build Version:3.6.2,45,!ELFs present; hard to build at version_3.6.2,subdir=geopaparazzi.app

Auto Update Mode:None
Update Check Mode:Tags
Current Version:3.6.2
Current Version Code:45

