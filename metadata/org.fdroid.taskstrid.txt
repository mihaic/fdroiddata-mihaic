Disabled:Still various non-free issues to be resolved
Categories:Office
License:GPLv2
Web Site:http://gitorious.org/astrid-foss
Source Code:http://gitorious.org/astrid-foss
Issue Tracker:

Auto Name:TaskStrid
Summary:Task management tool
Description:
A task management tool, based on the popular Astrid. It comes with features like reminders, tagging and
widgets, but without Astrid features such as ads and proprietary add-ons.
.

Repo Type:git
Repo:git://gitorious.org/astrid-foss/astrid-foss.git

Build Version:3.6.4,170,f583967d77a28f7a83392095d7edb1537dfe8a73,submodules=yes,prebuild=sed -i "s@\.\./astridApi@astridApi@" default.properties

Auto Update Mode:None
Update Check Mode:Static

