Categories:System
License:Apache2
Web Site:http://hybdms.blogspot.com.es/
Source Code:https://github.com/sukso96100/SidePanel
Issue Tracker:https://github.com/sukso96100/SidePanel
Donate:http://hybdms.blogspot.kr/p/donate-me.html

Auto Name:SidePanel
Summary:Side panel app launcher
Description:
Swipe to reveal panels on the side of the screen.
The panels contain the currently running applications, so that you can easily
keep track of them and enter any of them at any time easily.

Status: BETA
.

Repo Type:git
Repo:git://github.com/sukso96100/SidePanel.git

Build Version:0.8(beta),10,7662a6d3743,subdir=SidePanel,init=\
rm -rf bin gen ../SignedAPKs ../abs/bin ../abs/gen,update=.;../abs
Build Version:0.8.1(beta),11,d6f19c408db,subdir=SidePanel,init=\
rm -rf bin gen ../SignedAPKs ../abs/bin ../abs/gen,update=.;../abs
Build Version:0.8.2(beta),12,6f507f8c16f,subdir=SidePanel,init=\
rm -rf bin gen ../SignedAPKs ../abs/bin ../abs/gen,update=.;../abs
Build Version:0.8.3(beta),13,95f612af4609c,subdir=SidePanel,target=android-17,init=\
rm -rf bin gen ../SignedAPKs ../abs/bin ../abs/gen,update=.;../abs;../StandOut

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.8.3(beta)
Current Version Code:13

