Categories:System
License:GPLv3
Web Site:http://wififixer.wordpress.com
Source Code:https://github.com/Zanshinmu/Wifi-Fixer
Issue Tracker:https://github.com/Zanshinmu/Wifi-Fixer/issues
Donate:http://wififixer.wordpress.com/donate/

Auto Name:Wifi Fixer
Summary:Resets broken WiFi connections
Description:
Detects and resets a broken WiFi connection.
.

Repo Type:git
Repo:https://github.com/Zanshinmu/Wifi-Fixer.git

Build Version:0.7.0.3.3,620,a3c0dfd8507a7778c1a3,subdir=wififixer,target=android-8
Build Version:0.8.0,758,d1278bb9b2cddb66a617,subdir=wififixer,target=android-8
Build Version:0.8.0.1,759,55394716804479e76d5f,subdir=wififixer,target=android-8
Build Version:0.8.0.2,760,ff612a31874fc89cb124,subdir=wififixer,target=android-8
Build Version:0.8.0.3.1,770,59b8e66a0409a5d4c6a6,subdir=wififixer,target=android-8
Build Version:0.8.0.5,822,846b5c057e886f8436eb,subdir=wififixer,target=android-8
Build Version:0.8.0.6,838,81e9557f73d8529755cd,subdir=wififixer,target=android-8
Build Version:0.9.0.1,915,!commit 7155ca938dc1 but can't compile for some reason
Build Version:0.9.0.2,922,b354e4538bf242f10125af49050e36126b668972,subdir=wififixer,target=android-15
Build Version:0.9.5,952,4d6dda4c9aa283c162fd1d4c3edaeb8c0d6d8a32,subdir=wififixer,target=android-15
Build Version:0.9.5.1,955,a1cd53e3d2183,subdir=wififixer,target=android-15
Build Version:0.9.5.2,959,278b682fddca4e34416f0f49ddf33b9d6dec7819,subdir=wififixer,target=android-15
Build Version:0.9.5.3,970,e1c737b53ebae7368b0f24c90593fe493b126edd,subdir=wififixer,target=android-15
Build Version:0.9.5.4,978,!can't update yet: patched support jar,subdir=wififixer,target=android-15
Build Version:0.9.5.5,980,d91f8cb77,subdir=wififixer,target=android-17
Build Version:0.9.5.6,983,d59123b438fe,subdir=wififixer,target=android-17
Build Version:1.0.1,1048,release_1.0.1,subdir=wififixer,gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0.1
Current Version Code:1048

