Categories:Office
License:GPLv3+
Web Site:http://mirakel.azapps.de
Source Code:https://github.com/azapps/mirakel-android
Issue Tracker:https://github.com/azapps/mirakel-android/issues
Donate:http://mirakel.azapps.de/help_us.html#donate
FlattrID:2188714

Name:Mirakel
Auto Name:Mirakel
Summary:Decentralized TODO list
Description:
Mirakel is a simple but powerful tool for managing your TODO-lists.
You can sync your lists with your own server! Features:

*  Manage your tasks in lists
*  Simple, but powerful UI with Tablet support
*  Nice little widget
*  Sort your tasks in different ways
*  Fully configurable
*  Notifications & Reminders
*  Easy Backup and Import
*  Import your tasks from Astrid and Any.Do
*  Sync the lists with your own (or the developers') server (→Experimental!)
.

Repo Type:git
Repo:https://github.com/azapps/mirakel-android.git

Build:1.0.0,2
    commit=v1.0

Build:1.0.1,3
    commit=v1.0.1.1

Build:1.1.0,4
    commit=v1.1.0

Build:1.1.1,5
    commit=v1.1.1

Build:2.0,6
    commit=v2.0

Build:2.1,7
    commit=v2.1

Build:2.1.1,8
    commit=v2.1.1

Build:2.1.2,10
    commit=v2.1.2

Build:2.1.3,16
    commit=v2.1.3

Build:2.1.4,17
    commit=v2.1.4

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:2.1.4
Current Version Code:17

