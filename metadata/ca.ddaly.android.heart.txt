Categories:Office
License:GPLv3
Web Site:https://daryldy.github.com/Heart
Source Code:https://github.com/daryldy/Heart
Issue Tracker:https://github.com/daryldy/Heart/issues

Auto Name:Heart Observe
Summary:Track pulse and blood pressure
Description:
No description available
.

Repo Type:git
Repo:https://github.com/daryldy/Heart.git

Build:1.1,3
    commit=v1.1
    rm=custom_rules.xml
    srclibs=2:NumberPicker-SimonVT@1da4372;1:ActionBarSherlock@4.4.0
    prebuild=mv misc/reText/help.txt assets/misc/help.html && \
        mv misc/reText/about.txt assets/misc/about.html

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.1
Current Version Code:3

