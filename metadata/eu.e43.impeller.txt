Categories:Internet
License:Apache2
Web Site:http://impeller.e43.eu
Source Code:https://github.com/oshepherd/Impeller
Issue Tracker:https://e43oss.atlassian.net/browse/IMP

Auto Name:Impeller
Summary:Client for the Pump.io network
Description:
A client for [http://pump.io pump.io], 
a stream server for distributed social networking and microblogging. 
Register a [http://pump.io/tryit.html free account] at a participating website
and follow some [https://static.jpope.org/users.html people], 
before using the app.

Status: Alpha. Not all the functions of the web UI are available, such as
following people or viewing their profiles.
.

Repo Type:git
Repo:https://github.com/oshepherd/Impeller.git

Build:0.1,2
    commit=24cfa5

Build:0.2,3
    commit=v0.2

Build:0.2.1,4
    commit=v0.2.1

Build:0.3.0,6
    commit=v0.3.0

Build:0.3.1,7
    commit=v0.3.1

Build:0.3.2,8
    commit=v0.3.2

Build:0.3.3,9
    commit=v0.3.3

Build:0.3.4,10
    commit=v0.3.4

Build:0.4,11
    commit=v0.4

Build:0.4.1,12
    commit=v0.4.1

Build:0.4.3,15
    commit=v0.4.3

Build:0.4.4,16
    commit=v0.4.4
    init=rm jiraconnect-android-main/build.xml
    update=.;jiraconnect-android-main

Build:0.4.5,17
    commit=v0.4.5
    init=rm jiraconnect-android-main/build.xml
    update=.;jiraconnect-android-main

Build:0.4.6,4006
    commit=v0.4.6
    init=rm jiraconnect-android-main/build.xml
    update=.;jiraconnect-android-main

Build:0.4.7,4007
    commit=v0.4.7
    init=rm jiraconnect-android-main/build.xml
    update=.;jiraconnect-android-main

Build:0.5.1,5001
    commit=v0.5.1
    init=rm jiraconnect-android-main/build.xml
    update=.;jiraconnect-android-main

Build:0.5.2,5002
    commit=v0.5.2
    init=rm jiraconnect-android-main/build.xml
    update=.;jiraconnect-android-main

Build:0.5.3,5003
    commit=v0.5.3
    init=rm jiraconnect-android-main/build.xml
    update=.;jiraconnect-android-main

Build:0.5.5,5005
    commit=v0.5.5
    init=rm jiraconnect-android-main/build.xml
    update=.;jiraconnect-android-main

Build:0.6.0,6000
    commit=v0.6.0
    init=rm jiraconnect-android-main/build.xml
    update=.;jiraconnect-android-main

Build:0.6.1,6001
    commit=v0.6.1
    init=rm jiraconnect-android-main/build.xml
    target=android-18
    update=.;jiraconnect-android-main
    extlibs=android/android-support-v4.jar;oauth-signpost/signpost-core-1.2.1.2.jar
    prebuild=printf 'android.library.reference.1=jiraconnect-android-main' >> project.properties && \
        mkdir -p jiraconnect-android-main/libs && \
        cd jiraconnect-android-main/libs/ && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/acra-4.5.0.jar && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/apache-mime4j-0.5.jar && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/commons-io-2.4.jar && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/guava-r08.jar && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/httpclient-4.0.jar && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/httpcore-4.0-beta3.jar && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/httpmime-4.0-beta2.jar 

Build:0.6.2,6002
    commit=v0.6.2
    init=rm jiraconnect-android-main/build.xml
    target=android-18
    update=.;jiraconnect-android-main
    extlibs=android/android-support-v4.jar;oauth-signpost/signpost-core-1.2.1.2.jar
    prebuild=printf 'android.library.reference.1=jiraconnect-android-main' >> project.properties && \
        mkdir -p jiraconnect-android-main/libs && \
        cd jiraconnect-android-main/libs/ && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/acra-4.5.0.jar && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/apache-mime4j-0.5.jar && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/commons-io-2.4.jar && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/guava-r08.jar && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/httpclient-4.0.jar && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/httpcore-4.0-beta3.jar && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/httpmime-4.0-beta2.jar 

Build:0.7.0,7000
    commit=v0.7.0
    init=rm jiraconnect-android-main/build.xml
    target=android-18
    extlibs=android/android-support-v4.jar;oauth-signpost/signpost-core-1.2.1.2.jar
    prebuild=printf 'android.library.reference.1=jiraconnect-android-main' >> project.properties && \
        mkdir -p jiraconnect-android-main/libs && \
        cd jiraconnect-android-main/libs/ && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/acra-4.5.0.jar && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/apache-mime4j-0.5.jar && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/commons-io-2.4.jar && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/guava-r08.jar && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/httpclient-4.0.jar && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/httpcore-4.0-beta3.jar && \
        wget https://github.com/oshepherd/Impeller/raw/v0.6.0/jiraconnect-android-main/libs/httpmime-4.0-beta2.jar 

Build:0.8.2,8002
    commit=v0.8.2
    gradle=yes

Build:0.8.3,8003
    commit=v0.8.3
    gradle=yes

Maintainer Notes:
Although in auto mode, builds for 0.8.0 and 0.8.1 were skipped because source
code was not pushed to the upstream repo.
.

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.8.3
Current Version Code:8003

