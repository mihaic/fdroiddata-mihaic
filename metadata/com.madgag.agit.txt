Categories:Development
License:GPLv3
Web Site:https://github.com/rtyley/agit/wiki
Source Code:https://github.com/rtyley/agit
Issue Tracker:https://github.com/rtyley/agit/issues
FlattrID:799516

Auto Name:Agit
Summary:Read software source code
Description:
Clone remote git repositories onto your phone!
* Just like regular Git, the full history is stored for offline access.
* Animated Diffs - Watch diffs smoothly transition from Before to After.
* Supports git, http, https protocols. SSH for private repos is also possible
* Periodic sync enabled on Android 2.2 and later: latest commits fetched quarter-hourly.
Currently this is a 'read-only' client. You can clone & fetch, but not commit and push.
.

# Many maven artifacts are uploaded by same dev and thus should be built from source
Repo Type:git
Repo:https://github.com/rtyley/agit.git

Build Version:1.34,130300716,agit-parent-1.34,maven=yes,bindir=agit/target,novcheck=yes
Build Version:1.38,130400912,agit-parent-1.38,maven=yes,prebuild=\
sed -i '/agit-integration-tests/d' pom.xml,bindir=agit/target,novcheck=yes

Auto Update Mode:None
# auto methods don't work: vercode is date dependent
Update Check Mode:None
Current Version:1.38
Current Version Code:130400912

