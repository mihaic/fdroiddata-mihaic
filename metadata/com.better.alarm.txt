Categories:System
License:Apache2
Web Site:
Source Code:https://github.com/yuriykulikov/AlarmClock
Issue Tracker:https://github.com/yuriykulikov/AlarmClock/issues

Name:Simple Alarm Clock
Auto Name:Clock
Summary:Improved alarm clock
Description:
Smart alarm clock that includes many improvements over the stock alarm clock.
It does not have any extra features such
as dock mode or a world clock.
.

Repo Type:git
Repo:https://github.com/yuriykulikov/AlarmClock.git

Build Version:2.4.14,2414,2.4.14,srclibs=\
AndroidUtils@78f987a660;ACRA-Yuri@acra-4.4.0,prebuild=\
cp -r $$AndroidUtils$$/src/com/github src/com/ && \
cp -r $$ACRA-Yuri$$/src/main/java/org src/
Build Version:2.5.07,2507,2.5.07,srclibs=\
AndroidUtils@78f987a660;ACRA-Yuri@2ef9a1cac2106c0e862a42,prebuild=\
cp -r $$AndroidUtils$$/src/com/github src/com/ && \
cp -r $$ACRA-Yuri$$/src/main/java/org src/
Build Version:2.5.09,2509,2.5.09,srclibs=\
AndroidUtils@78f987a660;ACRA-Yuri@2ef9a1cac2106c0e862a42,prebuild=\
cp -r $$AndroidUtils$$/src/com/github src/com/ && \
cp -r $$ACRA-Yuri$$/src/main/java/org src/

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.5.09
Current Version Code:2509

