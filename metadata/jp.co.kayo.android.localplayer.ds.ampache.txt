Categories:Multimedia
License:GPLv2+
Web Site:http://justplayer-dev.blogspot.jp/
Source Code:https://bitbucket.org/yokmama/just-player-providers/src
Issue Tracker:https://groups.google.com/group/justplayer-user

Name:Just Player Plugin: Ampache
Auto Name:Ampache Provider
Summary:Ampache plugin for Just Player
Description:
This app allows you to add remote streaming functionality to [[jp.co.kayo.android.localplayer]], using an
[http://ampache.org Ampache] server e.g. via [http://www.owncloud.org Owncloud].
.

Repo Type:git
Repo:https://bitbucket.org/yokmama/just-player-providers

Build Version:1.19,410044,1370e4cfe884,subdir=AmpacheProvider
Build Version:1.22,410047,3155452,subdir=AmpacheProvider
Build Version:1.23,410048,be0fa93,subdir=AmpacheProvider
Build Version:1.24,410049,80ed67a,subdir=AmpacheProvider

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.24
Current Version Code:410049

