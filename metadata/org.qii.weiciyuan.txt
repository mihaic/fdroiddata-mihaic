Categories:Internet
License:GPLv3
Web Site:https://github.com/qii/weiciyuan/wiki
Source Code:https://github.com/qii/weiciyuan
Issue Tracker:https://github.com/qii/weiciyuan/issues

Auto Name:四次元
Summary:Sina Weibo client
Description:
Chinese social networking. Supposedly weibo.com has an English
translation now but the app doesn't yet.
.

Repo Type:git
Repo:https://github.com/qii/weiciyuan.git

Build Version:0.481,23,95a0e2e9aa88,rm=libs/android-support-v4.jar,extlibs=android/android-support-v4.jar
Build Version:0.483,25,00a434ba7,rm=libs/android-support-v4.jar,\
extlibs=android/android-support-v4.jar,prebuild=rm -rf libs/google-play-services_lib

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.483
Current Version Code:25

