AntiFeatures:Tracking
Categories:Office
License:GPLv3
Web Site:https://dev.zephyrsoft.org
Source Code:https://dev.zephyrsoft.org/svn/projects/trackworktime/trunk
Issue Tracker:https://dev.zephyrsoft.org/jira/browse/TWT

Auto Name:Track Work Time
Summary:Track your work time
Description:
It lets you categorize each recorded interval by a client/task and a free text. 
The list of clients/tasks can be edited to suit your needs.

Additionally, if you wish, your flexible time account is taken care of: 
you always see how much you worked. 
You can also keep an eye on how much work time is left for today (by a 
notification which you can enable).

You may provide the geo-coordinates of your work place and the app can 
automatically clock you in while you are at work. 
This is done without using GPS, so your battery won’t be emptied by this app.

Anti-feature: Tracking. Crash reports are sent without interaction.
.

Repo Type:git-svn
Repo:https://dev.zephyrsoft.org/svn/projects/trackworktime;trunk=trunk;tags=tags

Build:0.5.11,14
    commit=476
    forceversion=yes

Build:0.5.12,15
    disable=not a release yet

Maintainer Notes:
Bumps just after release
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.5.12-SNAPSHOT
Current Version Code:15

