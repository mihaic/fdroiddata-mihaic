Categories:Science & Education
License:GPL
Web Site:http://www.osciprime.com/index.php
Source Code:https://bitbucket.org/dalb8/osciiprime 
Issue Tracker:

Auto Name:OsciPrime
Summary:Oscilloscope to measure signals
Description:
Measure the microphone audio input as well as a USB Oscilloscope on
sale from the website. 
.

Repo Type:hg
Repo:https://bitbucket.org/dalb8/osciiprime

# already a prebuilt in jni/ but use NDK version instead
Build Version:Dagobert,14,0,extlibs=android/android-support-v4.jar,\
scanignore=build/ch.nexuscomputing.android.osciprimeics/jni,\
prebuild=cp $$NDK$$/platforms/android-14/arch-arm/usr/lib/liblog.so jni/,\
buildjni=yes

Auto Update Mode:None
#Update Check Mode:Market
Update Check Mode:None
Current Version:Dagobert
Current Version Code:14

