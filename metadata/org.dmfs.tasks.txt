AntiFeatures:NonFreeAdd
Categories:Office
License:Apache2
Web Site:https://github.com/dmfs/tasks#tasks
Source Code:https://github.com/dmfs/tasks
Issue Tracker:https://github.com/dmfs/tasks/issues

Auto Name:Tasks
Summary:Keep track of your list of goals
Description:
A simple task manager app, allowing you to categorise your todo list by
urgency, state, timeframe etc.
It is supposed to synchronise to a CalDAV server but this only works via
the author's proprietary app.
There isn't an option to export the data: therefore you must archive 
/data/app/org.dmfs.tasks as root; or use adb backup if you are on Android 4.

Anti-feature: Non-free addons. Your data can't be synchronised or exported 
unless you install another, proprietary app.

Status: Beta.
.

Repo Type:git
Repo:https://github.com/dmfs/tasks.git

Build Version:1.0.3,10,f9d88f8280,extlibs=android/android-support-v4.jar,\
srclibs=task-provider@4b402e7118,prebuild=\
sed -i 's@\(reference.1=\).*@\1$$task-provider$$@' project.properties 
Build Version:1.0.5,15,1.0.5,extlibs=android/android-support-v4.jar,\
srclibs=task-provider@c14e63ea9c,init=rm -r releases,prebuild=\
sed -i 's@\(reference.1=\).*@\1$$task-provider$$@' project.properties 

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0.5
Current Version Code:15

