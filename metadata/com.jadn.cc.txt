AntiFeatures:UpstreamNonFree,Tracking
Categories:Multimedia
License:MIT
Web Site:http://jadn.com/carcast/
Source Code:http://github.com/bherrmann7/Car-Cast/
Issue Tracker:https://github.com/bherrmann7/Car-Cast/issues

Auto Name:Car Cast
Summary:Simple podcast downloader
Description:
Car Cast is a simple audio podcast downloader and player.
Optimized for use in a daily commute; it features big buttons, large text and remembers the
last played location.

# Subscribe to podcasts by searching or adding feed urls (opml in next version)
# Download 1,2,3 etc. most recent podcasts for each feed
# Playback in car/gym (no network needed)

Admob was removed before building.
Anti-feature: Tracking. Usage data is sent by default though it can be disabled in settings.
.

Repo Type:git
Repo:git://github.com/bherrmann7/Car-Cast.git

Build:1.0.129,129
    commit=7a879c6bfa51b5d80401b84e031bf4ff2981bb8c
    subdir=cc
    target=android-8
    rm=cc/libs/admob-sdk-android.jar
    patch=admob.patch

#Note that the above patch no longer applies
Build:1.0.131,131
    disable=Contains proprietary binaries - needs patching or disabling
    commit=unknown - see disabled
    subdir=cc

Build:1.0.143,143
    commit=4b62e71e52
    subdir=cc
    srclibs=MobAdMob@2d5736
    prebuild=echo "android.library.reference.1=$$MobAdMob$$" >> project.properties && \
        rm libs/GoogleAdMobAdsSdk-6.0.0.jar

Build:1.0.154,154
    commit=b8bbc8e4fd
    subdir=cc
    init=rm ../PrestoClient/build.xml
    update=.;../PrestoClient
    rm=cc/libs/GoogleAdMobAdsSdk-6.0.0.jar
    srclibs=MobAdMob@2d5736
    prebuild=echo "android.library.reference.2=$$MobAdMob$$" >> project.properties

Build:1.0.155,155
    disable=no clear commit for this version

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0.155
Current Version Code:155

