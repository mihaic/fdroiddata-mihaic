Categories:Office
License:Apache2
Web Site:https://github.com/anysoftkeyboard
Source Code:http://softkeyboard.googlecode.com/svn/trunk/LanguagePacks/Catalan
Issue Tracker:https://github.com/AnySoftKeyboard/LanguagePack/issues
Donate:http://code.google.com/p/softkeyboard

Name:AnySoftKeyboard: Catalan
Auto Name:AnySoftKeyboard - Catalan Language Pack
Summary:Language pack for AnySoftKeyboard
Description:
Dictionary is based on the Wikipedia article database
which can be distributed under the terms of either the GNU Free Documentation
or the Creative Commons Attribution-ShareAlike licenses and
has about 5,000 words.

Install [[com.menny.android.anysoftkeyboard]] first, then select
the desired layout from AnySoftKeyboard's Settings->Keyboards menu.
.

Repo Type:git-svn
Repo:http://softkeyboard.googlecode.com/svn/trunk/LanguagePacks/Catalan

Build Version:20110220,3,1560,patch=xml.patch,\
update=force,init=rm -rf bin/ gen/,extlibs=LanguagePacks/ca.xml.gz,\
srclibs=AnySoftKeyboard-API@b21d8907;AnySoftKeyboardTools@73e9a09496,prebuild=\
sed -i 's@\(android.library.reference.1=\).*@\1$$AnySoftKeyboard-API$$@' project.properties && \
mkdir -p dict/ res/raw/ && gunzip -c libs/ca.xml.gz > dict/words.xml && rm -rf assets/ libs/,\
build=java -jar $$AnySoftKeyboardTools$$/makedict/makedict.jar

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:20110220
Current Version Code:3

