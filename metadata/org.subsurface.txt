Categories:Science & Education
License:GPL
Web Site:http://subsurface.hohndel.org
Source Code:http://git.hohndel.org/index.cgi?p=subsurface.git;a=summary
Issue Tracker:http://trac.hohndel.org/report/1

Auto Name:Subsurface
Summary:Dive logger
Description:
Companion app for the cross-platform Subsurface desktop app

* Capture dive positions
* Search and modify old dives
* Synchronise dives with a server for manipulation in desktop app
.

Repo Type:git
Repo:git://subsurface.hohndel.org/subsurface-companion.git

Build Version:1.7,8,1.7,maven=yes,bindir=target,prebuild=\
sed -i '/sherlock/d' project.properties

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.7
Current Version Code:8

