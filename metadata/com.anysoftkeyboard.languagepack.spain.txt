Categories:Office
License:GNUFDL/CCBYSA
Web Site:https://github.com/anysoftkeyboard
Source Code:http://softkeyboard.googlecode.com/svn/trunk/LanguagePacks/Spain
Issue Tracker:https://github.com/AnySoftKeyboard/LanguagePack/issues
Donate:http://code.google.com/p/softkeyboard

Name:AnySoftKeyboard: Spanish
Auto Name:AnySoftKeyboard - Spain Language Pack
Summary:Language pack for AnySoftKeyboard
Description:
Dictionary is based on the Wikipedia article database
which can be distributed under the terms of either the GNU Free Documentation
or the Creative Commons Attribution-ShareAlike licenses. 
It has about 200,000 words and the smaller version has 80,000.

Install [[com.menny.android.anysoftkeyboard]] first, then select
the desired layout from AnySoftKeyboard's Settings->Keyboards menu.
.

Repo Type:git-svn
Repo:http://softkeyboard.googlecode.com/svn/trunk/LanguagePacks/Spain

Build Version:20110717-smaller,2,1813,forceversion=yes,forcevercode=yes,\
patch=xml.patch;dump.patch,extlibs=LanguagePacks/es-smaller.xml.gz,\
srclibs=AnySoftKeyboard-API@b21d8907;AnySoftKeyboardTools@73e9a09496,prebuild=\
sed -i 's@\(.1=\).*@\1$$AnySoftKeyboard-API$$@' project.properties && \
mkdir -p dict/ res/raw/ && gunzip -c libs/es-smaller.xml.gz > dict/words.xml && \
rm -rf assets/,build=java -jar $$AnySoftKeyboardTools$$/makedict/makedict.jar
Build Version:20110717,3,1813,\
patch=xml.patch;dump.patch,extlibs=LanguagePacks/es.xml.gz,\
srclibs=AnySoftKeyboard-API@b21d8907;AnySoftKeyboardTools@73e9a09496,prebuild=\
sed -i 's@\(.1=\).*@\1$$AnySoftKeyboard-API$$@' project.properties && \
mkdir -p dict/ res/raw/ && gunzip -c libs/es.xml.gz > dict/words.xml && \
rm -rf assets/,build=java -jar $$AnySoftKeyboardTools$$/makedict/makedict.jar

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:20110717
Current Version Code:3

