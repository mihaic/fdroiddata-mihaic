Categories:System
License:MIT
Web Site:http://andsys.sourceforge.net
Source Code:http://sourceforge.net/p/andsys/git
Issue Tracker:http://sourceforge.net/p/andsys/discussion
Donate:http://sourceforge.net/p/andsys/donate

Auto Name:"AndSys ~ Apps"
Summary:Package information
Description:
* List all the applications installed on the device
* Order by: name, package, install date or permissions
* Filter launchable applications

Status: BETA
.

Repo Type:git
# Old repos
# Repo:git://git.code.sf.net/p/andsys/code
# Repo:https://github.com/orru/andsys
Repo:git://git.code.sf.net/p/andsys/git

# Older recipes are not available in the new repo
# Build Version:0.1.i.8,8,5acdcaf53985812a91ef03ae3d3ee97d1f466168,\
# prebuild=rm -rf bin/,target=android-10
# Build Version:0.1.l.11,11,124e49077a,init=rm -rf bin/ gen/
Build Version:0.2.a.12,12,065b79,init=rm -rf bin/ gen/
Build Version:0.2.d.15,15,5862e85a654c,init=rm -rf bin/ gen/
Build Version:0.3.e.21,21,03bdd75002cf040,init=rm -rf bin/ gen/
Build Version:0.3.f.22,22,665257030f60e82,init=rm -rf bin/ gen/
Build Version:0.3.g.23,23,091b33410f9b8bb,init=rm -rf bin/ gen/
Build Version:0.3.h.24,24,28e696e2216f9f8e75b4,init=rm -rf bin/ gen/
Build Version:0.3.i.25,25,6466312acd494ff438ed,init=rm -rf bin/ gen/
Build Version:0.3.l.28,28,bf93ce29c15432e,init=rm -rf bin/ gen/
Build Version:0.3.m.29,29,94a0ad045dafb9ec0,init=rm -rf bin/ gen/
Build Version:0.3.n.30,30,5c3cacbd90222d0e0,init=rm -rf bin/ gen/
Build Version:0.4-rc3,33,v0.4-rc3,srclibs=AndTheme@v0.0.3,\
init=\
    rm -rf bin/ gen/,\
prebuild=\
    echo "android.library.reference.1=$$AndTheme$$" >> project.properties

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.4-rc3
Current Version Code:33

