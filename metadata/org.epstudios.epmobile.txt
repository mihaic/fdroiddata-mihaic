Categories:Science & Education
License:GPLv3
Web Site:http://www.epstudiossoftware.com/blog/?page_id=6
Source Code:https://github.com/mannd/epmobile
Issue Tracker:https://github.com/mannd/epmobile/issues

Auto Name:EP Mobile
Summary:Medical tools
Description:
EP Mobile is an application that provides a set of tools for electrophysiologists and other health care
workers who deal with cardiac arrhythmias. The program includes EP calculators, drug dose calculators,
risk scores, diagnostic tools, ECG figures, and more.

[https://github.com/mannd/epmobile/tree/master/doc/changes Release Notes].
.

Repo Type:git
Repo:https://github.com/mannd/epmobile.git

Build Version:1.2,18,74c857
Build Version:1.3,19,v1.3
Build Version:1.3.1,20,v1.3.1
Build Version:1.5.1,24,v1.5.1
Build Version:2.0,25,v2.0
Build Version:2.0.1,26,v2.0.1
Build Version:2.1,27,v2.1
Build Version:2.2.1,29,v2.2.1

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.2.1
Current Version Code:29

