Categories:Games
License:Apache2
Web Site:https://github.com/boombuler/Shift
Source Code:https://github.com/boombuler/Shift
Issue Tracker:https://github.com/boombuler/Shift/issues

Auto Name:Shift
Summary:Eliminate the tiles
Description:
Shift is a small puzzle game for Android. It is a remake of a game
for WindowsMobile devices.
.

Repo Type:git
Repo:https://github.com/boombuler/Shift.git

Build Version:1.0.1,101,5d7784b359

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0.1
Current Version Code:101

