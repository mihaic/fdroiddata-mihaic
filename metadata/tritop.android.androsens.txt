Categories:System
License:GPLv3
Web Site:https://code.google.com/p/androsens
Source Code:https://code.google.com/p/androsens/source/list
Issue Tracker:

Auto Name:Androsens
Summary:Read data from sensors
Description:
Find submerged submarines or dig yourself out of avalanches by consulting
your phone; modern smartphones are equipped with a wide variety of sensors
and this application displays on a graph what those sensors are reading.
If you're not sure if the sensors on your smartphone are working you can use
this app to find out.
.

Repo Type:git-svn
Repo:http://androsens.googlecode.com/svn

#hard to find commit
Build Version:1.2,2,33

Auto Update Mode:None
#No version code found on market
Update Check Mode:RepoManifest
Current Version:1.2
Current Version Code:2

