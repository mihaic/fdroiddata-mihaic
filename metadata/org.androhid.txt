Disabled:Still not buildable, no code updates for three years
Category:System
License:GPLv3
Web Site:https://code.google.com/p/androhid/
Source Code:https://code.google.com/p/androhid/source/browse/
Issue Tracker:https://code.google.com/p/androhid/issues/list
Donate:https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=6R56TQAX8GTVQ

Auto Name:AndroHid
Summary:Bluetooth HID (keyboard) implementation
Description:
Use your android device as a bluetooth keyboard. Sending keystroke events over the bluetooth
HID protocol to your computer, allows you to remotely control your PC or Laptop. As the
bluetooth HID protocol is a standard specification which is provided by most modern operating
systems you don't have to install any further software on the host.
.

#Why?
Requires Root:Yes

Repo Type:svn
Repo:http://androhid.googlecode.com/svn/trunk/

Auto Update Mode:None
# needs a libbluetooth.so (e.g from CM zip) and headers from a device, then find a way for
# the ndk to find them without them actually being inside the ndk
#Build Version:0.6,6,34,buildjni=yes
#,patch=jni.patch
#not in the market; not updated since 10/2010
Update Check Mode:Static

