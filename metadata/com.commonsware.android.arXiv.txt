Categories:Reading
License:GPLv2
Web Site:http://code.google.com/p/arxiv-mobile
Source Code:http://code.google.com/p/arxiv-mobile/source/list
Issue Tracker:http://code.google.com/p/arxiv-mobile/issues/list

Auto Name:arXiv mobile
Summary:Client for arXiv.org
Description:
Keep up with the latest science research: physics, math, computers science.
Browse daily science articles (Physics, Astronomy, Math...) at arXiv.org and search the entire
arXiv collection. You can read PDFs, save them for later reading and share articles. A homescreen
widget lets you know of any new articles in your favorite science categories (dozens of physics,
math, computer science categories) or custom searches.
.

#Repo Type:bzr
#Repo:lp:arxivdroid
Repo Type:git-svn
Repo:http://arxiv-mobile.googlecode.com/svn/trunk

Build Version:2.0.4,90,!94 but doesn't compile due to apparently missing resource,target=android-11
Build Version:2.0.6,92,95,target=android-11,prebuild=sed -i -e "/key\.alias.*/d" -e "/key\.store.*/d" *.properties
Build Version:2.0.10,96,!No source in repo,target=android-11
Build Version:2.0.14,100,!No source in repo,target=android-11
Build Version:2.0.16,102,!No source in repo,target=android-11
Build Version:2.0.20,106,4,patch=fix.patch

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.0.20
Current Version Code:106

