Categories:Phone & SMS
License:GPLv3
Web Site:
Source Code:https://github.com/WhisperSystems/TextSecure
Issue Tracker:

Auto Name:TextSecure
Summary:SMS encryption
Description:
'''N.B'''ChatSafe is a re-branded version of TextSecure. The author
doesn't want the latter being distributed via f-droid.org. If
you have any problems with our build don't look for support from them.

ChatSafe is a drop-in replacement for the standard text messaging application
allowing you to send and receive text messages as normal. All text messages
sent or received with ChatSafe are stored in an encrypted database on your
phone, and text messages are encrypted during transmission when communicating
with someone else also using ChatSafe.
.

Repo Type:git
Repo:https://github.com/WhisperSystems/TextSecure

# TODO before release:
# * fix app_name in res/values-{ar,bo}/strings.xml
# * get/use new icons
# Important: Patch needs to be updated at every release.
Build Version:0.9.8,45,!a few things yet to do v0.9.8,target=android-17,patch=branding_v0.9.8.patch,\
srclibs=ActionBarSherlock@4.2.0,prebuild=\
cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/ && \
echo "android.jar=$$SDK$$/platforms/android-17/android.jar" > ant.properties && \
echo "android.library.reference.1=$$ActionBarSherlock$$" >> project.properties

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.9.8
Current Version Code:45

