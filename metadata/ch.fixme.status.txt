Categories:Office
License:GPLv2
Web Site:
Source Code:https://github.com/fixme-lausanne/MyHackerspace/
Issue Tracker:https://github.com/fixme-lausanne/MyHackerspace/issues

Auto Name:MyHackerspace
Summary:Hackerspace open/closed statuses
Description:
Open/Close statuses of the hackerspaces included in the SpaceAPI.
Show information included in the API and a widget is provided to show
it on the homescreen.
.

Repo Type:git
Repo:https://github.com/fixme-lausanne/MyHackerspace.git

Build Version:1.6.1,10,56287be60f,prebuild=rm -rf apks/
Build Version:1.7.1,14,e706cab0fd305,prebuild=rm -rf apks/
Build Version:1.7.2,15,069efc060c192cc3a3c30996bc,prebuild=rm -rf apks/

Build:1.7.3,16
	commit=b57229c55c3ff
	init=rm -rf apks/

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.7.3
Current Version Code:16

